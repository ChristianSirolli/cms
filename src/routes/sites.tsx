export default () => {
    return (<>
        <div class="ui five wide container">
            <div class="ui link centered cards" id="pageCards">
                <div class="ui card">
                    <div class="content">
                        <div class="center aligned header">Home</div>
                        <div class="center aligned meta">/</div>
                    </div>
                </div>
                <div class="ui card">
                    <div class="content">
                        <div class="center aligned header">Print QR Code</div>
                        <div class="center aligned meta">/print</div>
                    </div>
                </div>
                <div class="ui card">
                    <div class="content">
                        <div class="center aligned header">Check In/Out Serial Number</div>
                        <div class="center aligned meta">/sn</div>
                    </div>
                </div>
                <div class="ui card">
                    <div class="content">
                        <div class="center aligned header">Find Serial Number</div>
                        <div class="center aligned meta">/scans</div>
                    </div>
                </div>
            </div>
            <div class="ui divider"></div>
            <div class="ui link centered cards">
                <div class="ui card">
                    <div class="content">
                        <div class="center aligned header">Site Settings</div>
                        <div class="center aligned description">Change site properties and meta data.</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui overlay fullscreen modal" id="editorModal">
            <div class="ui top attached menu">
                <a href="/" class="ui icon item">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20">
                        <path d="M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z" />
                    </svg>
                </a>
                <a href="/print" class="ui icon item">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20">
                        <path id="printerIcon"
                            d="M19 8C20.66 8 22 9.34 22 11V17H18V21H6V17H2V11C2 9.34 3.34 8 5 8H6V3H18V8H19M8 5V8H16V5H8M16 19V15H8V19H16M18 15H20V11C20 10.45 19.55 10 19 10H5C4.45 10 4 10.45 4 11V15H6V13H18V15M19 11.5C19 12.05 18.55 12.5 18 12.5C17.45 12.5 17 12.05 17 11.5C17 10.95 17.45 10.5 18 10.5C18.55 10.5 19 10.95 19 11.5Z" />
                    </svg>
                </a>
                <a href="/scans" class="ui icon item">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20">
                        <path id="tableIcon"
                            d="M5,4H19A2,2 0 0,1 21,6V18A2,2 0 0,1 19,20H5A2,2 0 0,1 3,18V6A2,2 0 0,1 5,4M5,8V12H11V8H5M13,8V12H19V8H13M5,14V18H11V14H5M13,14V18H19V14H13Z" />
                    </svg>
                </a>
                <div class="right menu">
                    {/* <div id="searchBar" class="ui right aligned category search item">
                        <div class="ui icon input">
                            <input class="prompt" type="text" placeholder="Search..." autocomplete="off"
                                aria-autocomplete="none" />
                            <i class="search icon"></i>
                        </div>
                        <div class="results"></div>
                    </div> */}
                </div>
            </div>
            <iframe id="editorPreview" src="/"></iframe>
        </div>
        <script src="/js/dashboard.js"></script>
    </>)
};