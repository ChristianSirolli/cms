import Head from '../components/Head.js';
import Body from '../components/Body.js';
import { type FC } from 'hono/jsx';
const handler: FC<{ title?: string, path: string }> = ({ children, title, path }) => {
    return (<html lang="en-us">
        <Head title={title ? title + ' | CMS' : 'CMS'}></Head>
        <Body path={path}>{children}</Body>
    </html>);
};
export default handler;