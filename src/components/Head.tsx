import { type FC } from 'hono/jsx';
const handler: FC<{ title?: string }> = ({ children, title }) => {
    return (
        <head>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
            <meta name="theme-color" media="(prefers-color-scheme: light)" content="lightgray" />
            <meta name="theme-color" media="(prefers-color-scheme: dark)" content="gray" />
            <title>{title}</title>
            <link rel="icon" href="data:image/svg+xml;utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' style='background:white'%3E%3Cpath d='M13,3V9H21V3M13,21H21V11H13M3,21H11V15H3M3,13H11V3H3V13Z' /%3E%3C/svg%3E" />
            <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js"></script>
            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.9.3/dist/semantic.min.css" />
            <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.9.3/dist/semantic.min.js"></script>
            <link href="https://cdn.datatables.net/v/se/dt-1.13.6/b-2.4.2/r-2.5.0/datatables.min.css" rel="stylesheet" />
            <script src="https://cdn.datatables.net/v/se/dt-1.13.6/b-2.4.2/r-2.5.0/datatables.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/webp-converter-browser@latest/dist/index.min.js"></script>
            <link rel="stylesheet" type="text/css" href="/css/global.css" />
            <script src="/js/global.js"></script>
            {children}
        </head>
    )
}
export default handler;