import { type FC } from 'hono/jsx';
let Links: FC<{ path: string }> = ({children, path}) => (<>
    <a href="/" class={`${path == '/' ? 'active ' : ''}item`}>Home</a>
    <a href="/pricing" class={`${path == '/pricing' ? 'active ' : ''}item`}>Pricing</a>
    <a href="/docs" class={`${path == '/docs' ? 'active ' : ''}item`}>Support</a>
    <div class="ui dropdown item">
        Resources
        <i class="dropdown icon"></i>
        <div class="menu">
            <a href="https://www.whynopadlock.com/" class="item">Why No Padlock? (SSL/Certs)</a>
            <a href="https://mxtoolbox.com/" class="item">MXToolbox (Email)</a>
        </div>
    </div>
    {children}
</>);
export { Links };
const handler: FC<{ path: string }> = ({ children, path }) => {
    return (<>
        <header class={`ui large top fixed ${path == '/' ? 'hidden ' : ''}menu`}>
            <div class="ui container">
                <Links path={path} />
                <div class="right menu">
                    <div class="item">
                        <a class="ui button">Log in</a>
                    </div>
                    <div class="item">
                        <a class="ui primary button">Sign Up</a>
                    </div>
                </div>
                {children}
            </div>
        </header>
        <div class="ui vertical inverted sidebar menu left">
            <Links path={path} />
            <a class="item">Log in</a>
            <a class="item">Sign Up</a>
        </div>
    </>)
}
export default handler;