import { type FC } from 'hono/jsx';
import Header from './Header.js';
const handler: FC<{ path: string }> = ({ children, path }) => {
    return (
        <body class="pushable">
            <Header path={path}></Header>
            <div class="pusher" style={path != '/' ? 'padding-top: 5em;' : ''}>
                {children}
            </div>
        </body>
    )
}
export default handler;