$(document).ready(async () => {
    $('#editorPreview').css('width', `${window.innerWidth}px`).css('height', `${window.innerHeight - 50}px`);
});
$('#pageCards .card').on('click', (e) => {
    $('#editorPreview').attr('src', $(e.delegateTarget).children('.content').children('.meta').text());
    $('#editorPreview').on('load', (event) => {
        $($('#editorPreview')[0].contentWindow.document.head).append(`<style>
.container:not(body):hover {
border: 1px solid blue;
}
</style>`);
    })
    $('#editorModal').modal('show');
    e.preventDefault();
});