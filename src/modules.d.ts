declare module 'lokijs/src/loki-fs-structured-adapter.js' {
    export default class LokiFSStructuredAdapter implements LokiPersistenceAdapter {
        mode: string;
        dbref: Loki | null;
        dirtyPartitions: number[] | undefined;
        constructor();
        /**
         * Generator for constructing lines for file streaming output of db container or collection.
         *
         * @param {object=} options - output format options for use externally to loki
         * @param {int=} options.partition - can be used to only output an individual collection or db (-1)
         *
         * @returns {string|array} A custom, restructured aggregation of independent serializations.
         * @memberof LokiFsStructuredAdapter
         */
        public generateDestructured(options: { partition: number }): Generator<unknown, string | Array<string>, unknown>
        /**
         * Loki persistence adapter interface function which outputs un-prototype db object reference to load from.
         *
         * @param {string} dbname - the name of the database to retrieve.
         * @param {function} callback - callback should accept string param containing db object reference.
         * @memberof LokiFsStructuredAdapter
         */
        public loadDatabase(dbname: string, callback: Function): void
        /**
         * Recursive function to chain loading of each collection one at a time. 
         * If at some point i can determine how to make async driven generator, this may be converted to generator.
         *
         * @param {string} dbname - the name to give the serialized database within the catalog.
         * @param {int} collectionIndex - the ordinal position of the collection to load.
         * @param {function} callback - callback to pass to next invocation or to call when done
         * @memberof LokiFsStructuredAdapter
         */
        public loadNextCollection(dbname: string, collectionIndex: number, callback: Function): void
        /**
         * Generator for yielding sequence of dirty partition indices to iterate.
         *
         * @memberof LokiFsStructuredAdapter
         */
        public getPartition(): Generator
        /**
         * Loki reference adapter interface function.  Saves structured json via loki database object reference.
         *
         * @param {string} dbname - the name to give the serialized database within the catalog.
         * @param {object} dbref - the loki database object reference to save.
         * @param {function} callback - callback passed obj.success with true or false
         * @memberof LokiFsStructuredAdapter
         */
        public exportDatabase(dbname: string, dbref: object, callback: Function): void
        /**
         * Utility method for queueing one save at a time
         */
        public saveNextPartition(dbname: string, pi: Generator, callback: Function): void
    }
    // export = LokiFSStructuredAdapter;
}