export interface SiteData {
    title: string
    children?: any
}