import { type Context, Hono, type Next } from 'hono';
import { serve } from '@hono/node-server';
import { serveStatic } from '@hono/node-server/serve-static';
import { createRxDatabase, addRxPlugin, type RxDocument } from 'rxdb';
import { getRxStorageLoki } from 'rxdb/plugins/storage-lokijs';
import { RxDBDevModePlugin } from 'rxdb/plugins/dev-mode';
import { RxDBMigrationPlugin } from 'rxdb/plugins/migration';
import LokiFSStructuredAdapter from 'lokijs/src/loki-fs-structured-adapter.js'
import { randomUUID } from 'crypto';
import Layout from './routes/_layout.js';
import Index from './routes/index.js';
import Dashboard from './routes/dashboard.js';
import Sites from './routes/sites.js';
import { jwt, decode, sign, verify } from 'hono/jwt';

addRxPlugin(RxDBDevModePlugin);
addRxPlugin(RxDBMigrationPlugin);

const db = await createRxDatabase({
    name: 'cmsdb',
    storage: getRxStorageLoki({
        adapter: new LokiFSStructuredAdapter(),
    })
});

const userSchema = {
    version: 0,
    primaryKey: 'id',
    type: 'object',
    properties: {
        id: {
            type: 'string',
            maxLength: 100
        },
        username: {
            type: 'string'
        },
        hash: {
            type: 'number'
        }
    },
    required: ['id', 'name']
}

const siteSchema = {
    version: 0,
    primaryKey: 'id',
    type: 'object',
    properties: {
        id: {
            type: 'string',
            maxLength: 100
        },
        name: {
            type: 'string'
        },
        owner: {
            type: 'string'
        }
    },
    required: ['id', 'name']
}

const pageSchema = {
    version: 0,
    primaryKey: 'id',
    type: 'object',
    properties: {
        id: {
            type: 'string',
            maxLength: 100
        },
        name: {
            type: 'string'
        },
        site: {
            type: 'string'
        }
    },
    required: ['id', 'name']
}

await db.addCollections({
    sites: {
        schema: siteSchema
    },
    pages: {
        schema: pageSchema
    },
    users: {
        schema: userSchema
    }
});

const app = new Hono({ strict: false });

app.use('*', async (c: Context, next: Next) => {
    c.setRenderer((content) => {
        return c.html(<Layout title={c.get('title')} path={c.req.path}>{content}</Layout>);
    });
    await next();
});

app.get('/', (c: Context) => {
    c.set('title', 'Index');
    return c.render(<Index />);
})
app.get('/dashboard', (c: Context) => {
    c.set('title', 'Dashboard');
    return c.render(<Dashboard />);
})
app.get('/sites', (c: Context) => {
    c.set('title', 'Sites');
    return c.render(<Sites />);
})

app.get('/*', serveStatic({
    root: './src/static/'
}));


app.post('/api/site', async (c: Context) => {
    let body = await c.req.json();
    let userID = c.get('jwtPayload').name;
    let user = await db.users.findOne({ selector: { id: userID } }).exec();
    db.sites.insert({
        id: randomUUID(),
        name: body.name,
        owner: userID
    })
});

app.use('/auth/*', jwt({ secret: 'it-is-very-secret' }))

app.get('/auth/page', (c: Context) => {
    const payload = c.get('jwtPayload');
    return c.json(payload);
})

const port = Number(process.argv[2]);
serve({ fetch: app.fetch, port: !isNaN(port) ? port : 3000 }, (info) => {
    console.log(`Listening on http://localhost:${info.port}`);
});